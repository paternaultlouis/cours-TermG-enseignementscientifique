#!/usr/bin/env python3

import numpy as np

# TP FP FN TN
def solve(ppv, tpr, fpr):
    a = np.array(
        [
            [1 - ppv, -ppv, 0, 0],
            [1 - tpr, 0, -tpr, 0],
            [0, 1 - fpr, 0, -fpr],
            [1, 1, 1, 1],
        ]
    )
    b = np.array([[0], [0], [0], [1]])
    return np.linalg.solve(a, b)


# Female
print("Proportions :", solve(0.797, 0.921, 0.148))
print("Nombre :", 1268*solve(0.797, 0.921, 0.148))
